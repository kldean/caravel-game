import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;  // Imports Java List utility.
import java.util.ArrayList; // Imports Java ArrayList utility .
/**
 * The results screen computes the percentage the individual user
 * selected a particular choice and displays that percentage to
 * the screen.  The results screen also computes
 * the percentage that all users selected a particular choice
 * and displays that percentage to the screen.
 * 
 * @author Kerry Dean 
 * @version (12 December 2018)
 */
public class ResultsScreen extends World
{
    /* INSTANCE VARIABLES / FIELDS */     
     
    private int safeSea;
    private int stormSea;
    private int island;
    private int warrior;
    private int rested;
    private int tired;
    private int safeSeaAllUsers;
    private int stormSeaAllUsers;
    private int islandAllUsers;
    private int warriorAllUsers;
    private int restedAllUsers;
    private int tiredAllUsers;
    private int totalPlaysAllUsers;
    private int safeSeaAllUsersPct;
    private int stormSeaAllUsersPct;
    private int islandAllUsersPct;
    private int warriorAllUsersPct;
    private int restedAllUsersPct;
    private int tiredAllUsersPct;
    private Results_background myBackground;
    /**
     * Constructor for objects of class ResultsScreen.
     * 
     */
    public ResultsScreen()
    {    
        // Create a new world with 600x600 cells with a cell size of 1x1 pixels.
        super(600, 600, 1); 
        displayUserResults();
        myBackground = new Results_background();
        addObject(myBackground, 290, 325 );
    }// end ResultsScreen constructor
    
    /**
     * Display the results of user choices and total percentage of 
     * individual choices that were selected.
     */
    UserInfo myInfo = UserInfo.getMyInfo();    
    public void displayUserResults()
    {
        // Initialize local variables to be utilized for percentage calculations.
        safeSea = myInfo.getInt( 0 );
        stormSea = myInfo.getInt( 1 );
        island = myInfo.getInt( 2 );
        warrior = myInfo.getInt( 3 );
        rested = myInfo.getInt( 4 );
        tired = myInfo.getInt( 5 ); 
        
        int totalNumberOfPlays = safeSea + stormSea + island + warrior + rested + tired;        
        // Calculate user's percentage of each individual choice.        
        double safeSeaPercentage =
            Math.round(100.0 * safeSea / totalNumberOfPlays );                               
        double stormSeaPercentage =
            Math.round(100.0 * stormSea / totalNumberOfPlays );                                 
        double islandPercentage =
            Math.round(100.0 * island / totalNumberOfPlays );                                
        double warriorPercentage =
            Math.round(100.0 * warrior / totalNumberOfPlays );                                
        double restedPercentage =
            Math.round(100.0 * rested / totalNumberOfPlays );                                
        double tiredPercentage =
            Math.round(100.0 * tired / totalNumberOfPlays );       
        
        showText( "You Chose CalmSea % ", 240, 150 );
        showText( "You Chose StormSea % ", 240, 175 );
        showText( "You Chose Non-Combat % ", 240, 200 );
        showText( "You Chose Battle % ", 240, 225 );
        showText( "You Chose Rested Crew % ", 240, 250 );
        showText( "You Chose Exhausted Crew % ", 240, 275 );
        
        showText( "" + safeSeaPercentage, 440, 150 );
        showText( "" + stormSeaPercentage, 440, 175 );
        showText( "" + islandPercentage, 440, 200 );
        showText( "" + warriorPercentage, 440, 225 );
        showText( "" + restedPercentage, 440, 250 );
        showText( "" + tiredPercentage, 440, 275 );
        
        safeSeaAllUsers = 0;
        stormSeaAllUsers = 0;
        islandAllUsers = 0;
        warriorAllUsers = 0;
        restedAllUsers = 0;
        tiredAllUsers = 0;
        
        List<UserInfo> allUsers = new ArrayList( myInfo.getTop( 0 )); // Retrieve a list of individual choices for all users.
                                                                      // For Loop coded with assistance of Dr. Canada.  
        for ( UserInfo currentUserInfo : allUsers )
        {
            safeSeaAllUsers += currentUserInfo.getInt( 0 );
            stormSeaAllUsers += currentUserInfo.getInt( 1 );
            islandAllUsers += currentUserInfo.getInt( 2 );
            warriorAllUsers += currentUserInfo.getInt( 3 );
            restedAllUsers += currentUserInfo.getInt( 4 );
            tiredAllUsers += currentUserInfo.getInt( 5 );            
        }// end for loop
        totalPlaysAllUsers = safeSeaAllUsers + stormSeaAllUsers + 
                             islandAllUsers + warriorAllUsers +
                             restedAllUsers + tiredAllUsers;
                             
        double safeSeaAllUsersPct = 
            Math.round(100.0 * safeSeaAllUsers / totalPlaysAllUsers);
        double stormSeaAllUsersPct = 
            Math.round(100.0 * stormSeaAllUsers / totalPlaysAllUsers);                     
        double islandAllUsersPct = 
            Math.round(100.0 * islandAllUsers / totalPlaysAllUsers);                     
        double warriorAllUsersPct = 
            Math.round(100.0 * warriorAllUsers / totalPlaysAllUsers);
        double restedAllUsersPct = 
            Math.round(100.0 * restedAllUsers / totalPlaysAllUsers);
        double tiredAllUsersPct = 
            Math.round(100.0 * tiredAllUsers / totalPlaysAllUsers);
            
        showText( "All Chose CalmSea % ", 240, 350 );
        showText( "All Chose StormSea % ", 240, 375 );
        showText( "All Chose Non-Combat % ", 240, 400 );
        showText( "All Chose Battle % ", 240, 425 );
        showText( "All Chose Rested Crew % ", 240, 450 );
        showText( "All Chose Exhausted Crew % ", 240, 475 );    
        
        showText( "" + safeSeaAllUsersPct, 440, 350 );
        showText( "" + stormSeaAllUsersPct, 440, 375 );
        showText( "" + islandAllUsersPct, 440, 400 );
        showText( "" + warriorAllUsersPct, 440, 425 );
        showText( "" + restedAllUsersPct, 440, 450 );
        showText( "" + tiredAllUsersPct, 440, 475 );        
    }// end method display user results
}// end class ResulstsScreen
