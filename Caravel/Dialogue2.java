import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Dialogue2 object is proposed question for SeaWorld2.
 * 
 * @author Kerry Dean 
 * @version (12 December 2018)
 */
public class Dialogue2 extends Actor
{
    /**
     * Act - Dialogue2 is simply text. It doesn't do anything.
     */
    public void act() 
    {
        // Add your action code here.
    }// end method act    
}// end class Dialogue2
