import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Results_background is dark background for ResultsScreen.
 * 
 * @author Kerry Dean 
 * @version (12 December 2018)
 */
public class Results_background extends Actor
{
    /**
     * Act - do whatever the Results_background wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }// end method act    
}// end class Results_background
