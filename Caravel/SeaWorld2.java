import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * SeaWorld2 contains the second set of choices presented to the user in 
 * the game scenario.
 * 
 * @author Kerry Dean 
 * @version (12 December 2018)
 */
public class SeaWorld2 extends World
{
    /* INSTANCE VARIABLES */
    private Ship myShip;
    private Island myIsland;
    private Warrior myWarrior;
    private Dialogue2 myDialogue2;
    /**
     * Constructor for objects of class SeaWorld2.
     * 
     */
    public SeaWorld2()
    {    
        // Create a new world with 600x600 cells with a cell size of 1x1 pixels.
        super(600, 600, 1); 
         

        myShip = new Ship();
        addObject(myShip,300,400);
        myIsland = new Island();
        addObject(myIsland,100, 400);
        myWarrior = new Warrior();
        addObject(myWarrior, 500, 400);
        myDialogue2 = new Dialogue2();
        addObject(myDialogue2, 300, 100); 
    }// end SeaWorld2 constructor
}// end Class SeaWorld2
