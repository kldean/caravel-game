import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Ship moves the ship icon throughout the World subclasses.
 * Ship transitions the game through each scenario and records
 * choices made by individual players.
 * 
 * @author Kerry Dean 
 * @version (12 December 2018)
 */
public class Ship extends Actor
{
    /* INSTANCE VARIABLES */
    private GreenfootImage image1;
    private GreenfootImage image2;
    private int choice1;
    private int choice2;
    private int choice3;
    private int choice4;
    private int choice5;
    private int choice6;

    /** Create a ship and initialize the two possible images.
     *  Declares local variables.
     * 
     */
    public Ship()
    {
        image1 = new GreenfootImage("boat02-f.png");
        image2 = new GreenfootImage("boat02.png");
        setImage(image1);
        choice1 = 0;
        choice2 = 0;
        choice3 = 0;
        choice4 = 0;
        choice5 = 0;
        choice6 = 0;

    }// end Ship constructor

    /**
     * Method to move ship to interact with choice objects. 
     * Once choice object is selected, the game will record choice
     * and move to next scene.
     * 
     */
    public void act() 
    {
        checkKeypress();
        checkForChoice();

    }// end method act 

    /** Method to check for the left or right arrow key press. 
     * If the right arrow key is pressed, the ship will move
     * two pixels to the right. If the left arrow key is pressed,
     * the ship will move two pixels to the left.
     */
    public void checkKeypress()
    {
        if   (   Greenfoot.isKeyDown( "left"))
        {
            setImage(image2);
            move (-2);
        }// end if

        if   (   Greenfoot.isKeyDown( "right"))
        {
            setImage(image1);
            move (+2);
        }// end if
    }// end method checkKeypress

    /** Method to check if the ship has intersected one of the choice 
     * actors.  If it has, then it records the choice and
     * switches to next scene of the game.
     */
    public void checkForChoice()
    {

        if ( isTouching(SafeSea.class) )
        {            
            choice1 = 1;
            choice2 = 0;
            if ( UserInfo.isStorageAvailable() ) // check to see if user is logged in
            {
                UserInfo myInfo = UserInfo.getMyInfo();
                myInfo.setInt( 0, myInfo.getInt(0) + 1 );
                myInfo.store();
            }// end INNER if
            Greenfoot.setWorld(new SeaWorld2()); // don't code any statements after this line      
        }// end OUTER if

        if ( isTouching(StormSea.class) )
        {
            choice1 = 0;
            choice2 = 1;
            if ( UserInfo.isStorageAvailable() ) // check to see if user is logged in
            {
                UserInfo myInfo = UserInfo.getMyInfo();
                myInfo.setInt( 1, myInfo.getInt(1) + 1 );
                myInfo.store();
            }// end INNER if
            Greenfoot.setWorld(new SeaWorld2());            
        }// end OUTER if

        if ( isTouching(Island.class) )
        {            
            choice3 = 1;
            choice4 = 0;
            if ( UserInfo.isStorageAvailable() ) // check to see if user is logged in
            {
                UserInfo myInfo = UserInfo.getMyInfo();
                myInfo.setInt( 2, myInfo.getInt(2) + 1 );
                myInfo.store();
            }// end INNER if
            Greenfoot.setWorld( new SeaWorld3() );            
        }// end OUTER if

        if ( isTouching(Warrior.class) )
        {
            choice3 = 0;
            choice4 = 1;
            if ( UserInfo.isStorageAvailable() ) // check to see if user is logged in
            {
                UserInfo myInfo = UserInfo.getMyInfo();
                myInfo.setInt( 3, myInfo.getInt(3) + 1 );
                myInfo.store();
            }// end INNER if
            Greenfoot.setWorld( new SeaWorld3() );            
        }// end OUTER if

        if ( isTouching(Rested.class) )
        {
            choice5 = 1;
            choice6 = 0;
            if ( UserInfo.isStorageAvailable() ) // check to see if user is logged in
            {
                UserInfo myInfo = UserInfo.getMyInfo();
                myInfo.setInt( 4, myInfo.getInt(4) + 1 );
                myInfo.store();
            }
            Greenfoot.setWorld(new ResultsScreen() );
        }        
        else if ( isTouching(Tired.class) )
        {
            choice5 = 0;
            choice6 = 1;
            if ( UserInfo.isStorageAvailable() ) // check to see if user is logged in
            {
                UserInfo myInfo = UserInfo.getMyInfo();
                myInfo.setInt( 5, myInfo.getInt(5) + 1 );
                myInfo.store();
            }// end INNER if
            Greenfoot.setWorld(new ResultsScreen() );
        }// end else-if        
    }// end method checkForChoice    
}// end Class Ship
