import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Dialogue3 object is proposed question for SeaWorld3.
 * 
 * @author Kerry Dean 
 * @version (12 December 2018)
 */
public class Dialogue3 extends Actor
{
    /**
     * Dialogue3 is simply text. It doesn't do anything.
     */
    public void act() 
    {
        // Add your action code here.
    }// end method act    
}// end class Dialogue3
