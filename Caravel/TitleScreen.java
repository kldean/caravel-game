import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * TitleScreen displays the title of the game, instructs
 * the user to log into Greenfoot, and lets the user
 * know to click the screen to begin the game.
 * 
 * @author Kerry Dean 
 * @version (12 December 2018)
 */
public class TitleScreen extends World
{    
    /**
     * Constructor for objects of class TitleScreen.
     * 
     */
    public TitleScreen()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 600, 1);         
    }// end TitleScreen constructor
    
    /**
     * This changes the TitleScreen to the SeaWorld scene and starts the 
     * the game when the player clicks the left mouse button 
     */
    public void act()
    {
        if ( Greenfoot.mouseClicked(this))
        {
            SeaWorld seaWorld = new SeaWorld();
            Greenfoot.setWorld ( seaWorld );
        }// end if
    }// end method act
}// end class TitleScreen
