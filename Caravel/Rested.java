import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Rested is an object of choice in SeaWorld3.
 * 
 * @author Kerry Dean 
 * @version (12 December 2018)
 */
public class Rested extends Actor
{
    /**
     * Act - do whatever the Rested wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }// end method act    
}// end class Rested
