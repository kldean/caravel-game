import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Dialogue1 object is proposed question for SeaWorld.
 * 
 * @author Kerry Dean 
 * @version (12 December 2018)
 */
public class Dialogue1 extends Actor
{
    /**
     * Act - Dialogue1 is simply text. It doesn't do anything.
     * .
     */
    public void act() 
    {
        // Add your action code here.
    }//  end method act    
}// end class Dialogue1