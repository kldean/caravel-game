import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * SeaWorld3 contains the final two choices of the game scenario.
 * 
 * @author Kerry Dean 
 * @version (12 December 2018)
 */
public class SeaWorld3 extends World
{
    /* INSTANCE VARIABLES */
    private Ship myShip;
    private Rested myRested;
    private Tired myTired;
    private Dialogue3 myDialogue3;
    /**
     * Constructor for objects of class SeaWorld3.
     * 
     */
    public SeaWorld3()
    {    
        // Create a new world with 600x 600 cells with a cell size of 1x1 pixels.
        super(600, 600, 1);        
         
         

        myShip = new Ship();
        addObject(myShip,300,400);
        myRested = new Rested();
        addObject(myRested,100, 400);
        myTired = new Tired();
        addObject(myTired, 500, 400);
        myDialogue3 = new Dialogue3();
        addObject(myDialogue3, 300, 100);
    }// end SeaWorld3 constructor
}// end class SeaWorld3
