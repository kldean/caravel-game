import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Island is an object of choice in SeaWorld2.
 * 
 * @author Kerry Dean 
 * @version (12 December 2018)
 */
public class Island extends Actor
{
    /**
     * Act - do whatever the Island wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }// end method act    
}// end class Island
