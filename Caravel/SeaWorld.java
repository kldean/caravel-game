import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Class SeaWorld creates the background world in which the game is to be played.
 * SeaWorld also contains the first set of choices presented to the user
 * in the game scenario.
 * 
 * @author Kerry Dean 
 * @version (12 December 2018)
 */
public class SeaWorld extends World
{
    /* INSTANCE VARIABLES */
    private Ship myShip;
    private SafeSea mySafeSea;
    private StormSea myStormSea;
    private Dialogue1 myDialogue1;

    /**
     * Constructor for objects of class SeaWorld.
     * 
     */
    public SeaWorld()
    {    
        // Create a new world with 600 x 600 cells with a cell size of 1x1 pixels.
        super(600, 600, 1); 

        myShip = new Ship();
        addObject(myShip,300,400);
        mySafeSea = new SafeSea();
        addObject(mySafeSea,100, 400);
        myStormSea = new StormSea();
        addObject(myStormSea, 500, 400);
        myDialogue1 = new Dialogue1();
        addObject(myDialogue1, 300, 100);

    }// end SeaWorld constructor      
}// end Class SeaWorld
